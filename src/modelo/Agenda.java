/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.ListIterator;
import java.io.*;
import java.util.StringTokenizer;

/**
 *
 * @author nacho
 */
public class Agenda {
    private ArrayList<Contacto> agenda= new ArrayList<>();
    private int numElementos=0;

    public ArrayList<Contacto> getAgenda() {
        return agenda;
    }

    public void setAgenda(ArrayList<Contacto> agenda) {
        this.agenda = agenda;
    }
    
    public boolean añadirContacto(Contacto c) {
        //boolean existe=false;
        
        /*for(Contacto c2:agenda){
            if(c2.getTelefono().equals(c.getTelefono())){
             existe=true;
             break;
            }
        } 
        if(!existe){
            agenda.add(c);
            return true;
        } else {
            return false;
        }*/
        if (!agenda.contains(c)) {
            agenda.add(c);
            return true;
        }
        return false;
        
    }
    
    // Devuelve el contacto que coincida con el nombre pasado como parámetro y
    // si no lo encuentra devuelve null
    public Contacto obtenerContactoApellido1(String apellido1) {
        Contacto c=null;
        ListIterator it=agenda.listIterator();
        while (it.hasNext()) {
            c=(Contacto)it.next();
            if (c.getApellido1().equals(apellido1)) {
                return c;
            }
        }
        return c;
    }
    
    // Devuelve el contacto que coincida con el nombre pasado como parámetro y
    // si no lo encuentra devuelve null
    public Contacto obtenerContactoTelefono(String telefono) {
        Contacto c=null;
        ListIterator it=agenda.listIterator();
        while (it.hasNext()) {
            c=(Contacto)it.next();
            if (c.getTelefono().equals(telefono)) {
                return c;
            }
        }        
        return c;
    }    

    @Override
    public String toString() {
        String cad;
        cad="Agenda\n";
        cad+="------\n";
        for (Contacto c:agenda) {
            cad+=c.toString()+"\n";
        }
        return cad;
    }
    
    public void desplazarDerecha(int e){
        
        for(int i=numElementos;i>e;i--){
            agenda.set(i-1,agenda.get(i-1));
        }
    }
    
    public boolean crearFichero() throws IOException {
        File f= new File("Agenda.txt");
        FileWriter fw = new FileWriter(f);
        for(Contacto c: agenda){
            fw.write(c.getNombre()+";");
            fw.write(c.getApellido1()+";");
            fw.write(c.getApellido2()+";");
            fw.write(c.getTelefono()+"\n");
        }
        fw.close();
        return true;
    }
    
    public boolean leerFichero() throws FileNotFoundException, IOException{
        agenda.clear();
        FileReader fr= new FileReader("Agenda.txt");
        String contacto="";
        int c=0;
        while( c!=-1 ) {
            contacto="";
            c=fr.read();
            while(c!=13 && c!=-1) {
                if (c!=13 && c!=10) {
                    contacto+=(char)c+"";
                }
                c=fr.read();
            }
            if (!contacto.equals("")) {
                añadeContacto(contacto);
            }
        }
        return true;
    }
    
    public void añadeContacto(String contacto) {
        StringTokenizer st= new StringTokenizer(contacto,";");
        String nombre=(String)st.nextToken(";");
        String apellido1=st.nextToken(";");
        String apellido2=st.nextToken(";");
        String telefono=st.nextToken(";");

        Contacto c=new Contacto(nombre, apellido1, apellido2, telefono);
        agenda.add(c);
    }
}
